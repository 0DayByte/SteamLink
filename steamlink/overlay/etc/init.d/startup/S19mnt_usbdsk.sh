#!/bin/sh
mkdir -p /mnt/disk
mount /dev/block/sda1 /mnt/disk
mkdir -p /home/apps/prboom-plus
mount /mnt/disk/storage/prboom-plus /home/apps/prboom-plus
mkdir -p /home/apps/kodi
mount /mnt/disk/storage/kodi /home/apps/kodi
mkdir -p /home/apps/mame4all
mount /mnt/disk/storage/mame4all /home/apps/mame4all
mkdir -p /home/apps/retroarch
mount /mnt/disk/storage/retroarch /home/apps/retroarch